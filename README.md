![](https://elioway.gitlab.io/eliosin/elio-Sin-logo.png)

> Style with no class, **the elioWay**

# eliosin

![alpha](https://elioway.gitlab.io/eliosin/icon/devops/alpha/favicon.ico "alpha")

A classless, wireframing CSS framework **the elioWay**.

**eliosin** curates, documents, and otherwise "hosts" its child modules **god** , **eve** , **adon** and **generator-sin**.

- [eliosin Documentation](https://elioway.gitlab.io/eliosin)

## Core

- [god](https://elioway.gitlab.io/eliosin/god)
- [eve](https://elioway.gitlab.io/eliosin/eve)
- [adon](https://elioway.gitlab.io/eliosin/adon)
- [generator-sin](https://elioway.gitlab.io/eliosin/generator-sin)

## Accessories

- [innocent](https://elioway.gitlab.io/eliosin/innocent)
- [toothpaste](https://elioway.gitlab.io/eliosin/toothpaste)

## Theme Packs

- [bushido](https://elioway.gitlab.io/eliosin/bushido)
- [sins](https://elioway.gitlab.io/eliosin/sins)

## Nutshell

- [eliosin Quickstart](https://elioway.gitlab.io/eliosin/quickstart.html)
- [eliosin Credits](https://elioway.gitlab.io/eliosin/credits.html)

![](https://elioway.gitlab.io/eliosin/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
