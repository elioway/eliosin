# eliosin FAQ

## What is sin?

Sin is the ideological belief that one's morality is determined by the extent to which your behaviour offends God.

## What is eliosin?

- **eliosin** is SASS framework, gridless, auto-scaling, purpose-driven, CSS-styled, HTML wireframe tool focused on tagNames, not classes. It is not intended as a replacement for Bootstrap or Pure or any "grid" system frameworks.

- **eliosin** is here so developers can quickly spin up a wireframe for their apps which look similar to Bootstrap, etc, without the HTML crud. Developers can use eliosin to immediately get focused on coding its features.

- **eliosin** is perfect for single page apps, prototypes or blog sites which don't need pretty complex layouts.

## Why is it a sin?

- Because massive, complicated CSS frameworks have become so prevalent, not using them has almost become a sin.

**eliosin** because I'm a web developer who sometimes prefers creating feature-rich applications over tweaking page-layouts. I hate it when I start a new project, eager with ideas for features, only to get bogged down first wireframing a webpage to work in. **eliosin** gives me a pattern I can return to.

There is an immediacy to eliosin that I hope other developers will appreciate.

## Why is it called eliosin?

- **the elioWay** is a cult, not a framework. **eliosin** borrows the language of a cult

- **eliosin** makes it clear which modules are the back bone of this framework: The modularity of **eliosin** is self-explained.
