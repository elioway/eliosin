> Why do I need to nest my `h1` tag, for instance, so deeply in so many `div` tags? Why do I need to nest my `nav` tag 3 deep inside a collection of `div` tags with classes like `.mainNav` and `.mainNavLeft`. When... oh when... oh when... oh when... do we ever need to use the className `.form` especially for the tagName `form`; or the className `.table` for a `table`? **Tim Bushell**

# We've lost our collective minds

<aside>
  <dl>
  <dd>Receive thy new possessor—one who brings</dd>
  <dd>A mind not to be changed by place or time.</dd>
  <dd>The mind is its own place, and in itself</dd>
  <dd>Can make a Heaven of Hell, a Hell of Heaven.</dd>
  <dd>What matter where, if I be still the same,</dd>
  <dd>And what I should be, all but less than he</dd>
  <dd>Whom thunder hath made greater? Here at least</dd>
  <dd>We shall be free</dd>
</dl>
</aside>

**eliosin** is an (unholy) trinity of open sourced libraries which demonstrate and implement _patterns_ for rapidly building workable CSS stylesheets which target tagNames, rather than classNames.

**eliosin** will work closely alongside **elioflesh** providing tiny, classless, css stylesheets.

We're creating stylesheets for wireframes which only mention tagNames. No divs. No class names. No rows and columns of nested tags. Just normal tags, **listed** _directly inside_ `body`, period.

[eliobones Dogma](/eliobones/dogma.html)

## Trinity

**eliosin** is split into three modules:

<section>
  <figure>
  <a href="/eliosin/god/artwork/splash.jpg" target="_splash"><img src="/eliosin/god/artwork/splash.jpg" target="_splash"></a>
  <h3>Structured</h3>
  <p>SASS <em>patterns</em> for wireframe or layout per tagName.</p>
  <p><a href="/eliosin/god"><button><img src="/eliosin/god/favicoff.png"/>god</button></a></p>
</figure>
  <figure>
  <a href="/eliosin/eve/artwork/splash.jpg" target="_splash"><img src="/eliosin/eve/artwork/splash.jpg" target="_splash"></a>
  <h3>Stylish</h3>
  <p>SASS <em>patterns</em> for CSS styling of individual tagNames.</p>
  <p><a href="/eliosin/eve"><button><img src="/eliosin/eve/favicoff.png"/>eve</button></a></p>
</figure>
  <figure>
  <a href="/eliosin/adon/artwork/splash.jpg" target="_splash"><img src="/eliosin/adon/artwork/splash.jpg" target="_splash"></a>
  <h3>Interactive</h3>
  <p>Javascript <em>patterns</em> for jquery/DOM UX actions per tagName.</p>
  <p><a href="/eliosin/adon"><button><img src="/eliosin/adon/favicoff.png"/>adon</button></a></p>
</figure>
</section>

**god**, **eve** and **adon** could easily be combined. By keeping them as separate libraries it clarifies certain facts:

1. The three libraries represent _patterns_ not features. You could just as easily look at the source code and implement your own version. I offer these _patterns_ as an extended "gist" you may repurpose as you wish. I stole my genius from other people anyway!

2. They are learning/teaching focused. They are small so that new developers can consume them easily.

3. Separated, their individual _patterns_ are clear. Metaphorically, the names **god**, **eve** and **adon** are well-known concepts which will help you organise their purpose in your mind.

4. Separate, they are optional, assuming you have the settings file in each.

> `body>X` in a stylesheet embodies everything you'd need to know about how to style `X`, **the elioWay**

**eliosin** is old school, and bemoans the thoughtless use of HTML tagNames, bypassed for the sake of classNames.

**eliosin** asks: Why do I need to nest my `h1` tag, for instance, so deeply in so many `div` tags? Why do I need to nest my `nav` tag 3 deep inside a collection of `div` tags with classes like `.mainNav` and `.mainNavLeft`. When... oh when... oh when... oh when... do we ever need to use the className `.form` especially for the tagName `form`; or the className `.table` for a `table`?

**eliosin** thinks we've lost our collective minds!

It is the considered opinion of **eliosin** that `body>X` in a stylesheet embodies everything you'd need to know about how to style `X`:

Simplistically, **eliosin** generated stylesheets look like this:

```css
body > h1 {
  /* style */
}
body > nav {
  /* style */
}
body > menu {
  /* style */
}
body > footer {
  /* style */
}
body > p {
  /* style */
}
```

## Related

<article>
  <a href="/eliosin/generator-sin/">
  <img src="/eliosin/generator-sin/favicoff.png"/>
  <div>
  <h4>generator-sin</h4>
  <p>yeoman generator for strict <strong>eliosin</strong> <em>patterns</em> for <strong>god</strong>, <strong>eve</strong> and <strong>adon</strong>, the <strong>elioWay</strong>.
</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/innocent/">
  <img src="/eliosin/innocent/favicoff.png"/>
  <div>
  <h4>innocent</h4>
  <p>Minimal acceptable class-less css <strong>eliosin</strong> theme, the <strong>elioWay</strong>.
  </p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/">
  <img src="/eliosin/bushido/favicoff.png"/>
  <div>
  <h4>bushido</h4>
  <p>8 elegant, ready made <strong>eliosin</strong> themes for news, blogs, shops, etc, targeted to a larger screen, the <strong>elioWay</strong>.
    </p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/sins/">
  <img src="/eliosin/sins/favicoff.png"/>
  <div>
  <h4>sins</h4>
  <p>7 snappy, ready made <strong>eliosin</strong> themes for interactive web apps with a mobile focus, the <strong>elioWay</strong>.
    </p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/toothpaste/">
  <img src="/eliosin/toothpaste/favicoff.png"/>
  <div>
  <h4>toothpaste</h4>
  <p>Puts listed tags alternatively into heaven, then hell, then heaven, then hell, the <strong>elioWay</strong>.
  </p>
</div>
</a>
</article>

## Tools

<article>
  <a href="/eliosin/sassy-fibonacciness/">
  <img src="/eliosin/sassy-fibonacciness/favicoff.png"/>
  <div>
  <h4>SassyFibonacciness</h4>
  <p>Fibonacci SASS mixin for weighting CSS units, especially heading font sizes, the <strong>elioWay</strong>.
</p>
</div>
</a>
</article>

## Useful

- [Tags Reference](cheat-tags.html)

## More

- [eliosin FAQ](/eliosin/faq.html)
- [the Prong Cookbook: Blogging App](/eliosin/god/the-prong-cookbook-1.html)
- [the Prong Cookbook: Classed Prongs](/eliosin/god/the-prong-cookbook-2.html)
