# eliosin Prerequisites

- [elioWay Prerequisites](/eliosin/prerequisites.html)

# qunit

Install qunit

```shell
npm install -g qunit
yarn global add qunit
```

## elioWay

```
npm i @elioway/god @elioway/eve @elioway/adon @elioway/innocent @elioway/sins
npm link @elioway/god @elioway/eve @elioway/adon @elioway/innocent @elioway/sins
```
