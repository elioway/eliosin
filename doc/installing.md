# Installing eliosin

- [Prerequisites](/eliosin/prerequisites.html)

## Install

You could go one or two of two routes:

- [Install generator-sin](/eliosin/generator-sin/installing.html)

  - **generator-sin** will install everything you need to play **god** in your current project folder. It will generate extra **eve**'s and **adon**'s when you need them.

- [Install god](/eliosin/god/installing.html)

  - Install **god** using yarn as a dependency of your current project.

  - Or install **god** with GIT to create a starting project.

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git submodule init
git submodule update
```

```
cd ~/Dev/elioway/eliosin/
set L sassy-fibonacciness god eve adon innocent bushido sins
set LRS ""
for R in $L
   cd ~/Dev/elioway/eliosin/$R
   echo "!!!!" @elioway/$R
   npm link
   for LR in $LRS
      npm link @elioway/$LR
      echo @elioway/$LR
    end
    set LRS $LRS $R
cd
```
