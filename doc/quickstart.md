# Quickstart eliosin

- [eliosin Prerequisites](/eliosin/prerequisites.html)
- [Installing eliosin](/eliosin/installing.html)

## Nutshell

For learning how to use the **eliosin** framework, see **god**'s own documentation:

- [Quick Start with god](/eliosin/god/quickstart.html)

If you think you already know what's going on here... you could just start here.

- [Quick Start with generator-sin](/eliosin/generator-sin/quickstart.html)

## Building documentation

Are you a developer changed with building eliosin documentation? This is right place.

Start by using yarn or npm to install the packages.

Run `prettier` using the `npm run prettier` command. This will format any code, including the MARKDOWN files in your doc folder.:

```shell
cd ~/repo/elio/eliosin
yarn
npm run prettier
```

Next, make sure all the **god**, **eve**, **adon** and **innocent** have been deployed:

```shell
cd ~/repo/elio/eliosin/god
gulp deploy
cd ~/repo/elio/eliosin/adon
gulp deploy
cd ~/repo/elio/eliosin/eve
gulp deploy
cd ~/repo/elio/eliosin/innocent
gulp deploy
```

Next, make sure all the **rei** and **sloth** themes have been deployed:

```shell
cd ~/repo/elio/eliosin/bushido/rei
gulp deploy
cd ~/repo/elio/eliosin/sins/sloth
gulp deploy
```

This project hosts **god**, **eve**, **adon** and **generator-sin** as well as the theme packs **sins** and **bushido**, all of which should be submodules of **eliosin**. Building documentation **the elioWay** is done by calling **chisel**. Calling `chisel` builds the documentation into the public folder.

```shell
cd ~/repo/elio/eliosin
chisel
```
