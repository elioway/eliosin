# TEW Dogma

- **god** shall only be used for page layout. _Style your own forms_.
- **god** layouts shall target tagNames not classNames.
- _pillar_ _hell_ _heaven_ shall be siblings of the same _container_.
- _pillar_ tags shall be laid-out as an equal-width, centred column.
- _hell_ tags shall be laid-out left and _heaven_ tags shall be laid-out right.
- No _hell_ _heaven_ shall be laid-out higher on the page than the _following_ sibling _pillar_ in the HTML source (examples coming).
- _pillar_ content shall wrap when overlapping _hell_ _heaven_.

**god** styled HTML should not be nested and needs no extraneous `div` tags. **god** styled HTML should be flat and naked.

## Prelude

### Prelude 1: HTML blocks and inlines

Just a reminder: In HTML, tags are either block or inline:

- **Block tags** like `p`, `h1`, `h2`, `div`, etc., typically force a line break (i.e. their contents will start on a new line).

  - _In MS Word terms: they are for paragraph formatting._

- **Inline tags** like `a`, `strong`, `em`, etc. typically change text.

  - _In MS Word terms: they are for text formatting._

  **god** treats all _believers_ as block tags.

### Prelude 2: Wireframe

By wireframe we mean any CSS styles which takes care of the page structure. A wireframe relates to the way content appears on virtually every webpage. Blogs, _bootscraps_, wordpresses, even many corporate sites - typically have pages which generally have the same page structure:

1. Content down the ~middle in a ~60% width column.
2. Blobs of content (adverts, links, facts, quotes, sidebars, photos, etc) scattered in the gutters either side.
3. A menu and footer 100% across the page top and bottom.

In **eliosin** it's **god**'s job to do that layout.

> There has to be an easier way than this tsunami of classNames and nested div tags.

## god vs bootscrap

<figure>
  <img src="/eliosin/icon/flood/artwork/splash.jpg" target="_splash" alt="the flood">
</figure>

Why **eliosin** can help is illustrated by a comparison.

Under Bootstrap, Pure, CSS Flex, Material, Tailwind, and many other CSS frameworks - which I will lovingly label under the umbrella name _bootscrap_ - a wireframe might be implemented like this:

**Figure 1**

```html
<link src="bootscrap.css" />

<div class="container mainpage mumbo-jumbo">
  <div class="row black no-margins no-end-in-sight parent-of-parent-of-parent">
    <div class="col3 padded squared nested">
      <nav class="sidebarnav dilly-dally"><a>Home</a></nav>
      <!-- content left column -->
    </div>
    <div class="col7 thick-line strong stand-out rounded">
      <h1>Content heading</h1>
      <!-- content centre -->
      <p>Content paragraph.</p>
      <!-- content centre -->
    </div>
    <div class="col2 despair shoot-me-now">
      <img src="advert.svg" />
      <!-- content right column -->
    </div>
  </div>
</div>
```

_NB: Five of the nine opening tags add no content._

Of the HTML in their care, such as with the example above, _bootscrap_ stylesheets require both structure _and_ classNames (see Figure 1) to create a 60% width column of text on the page with blobs of content scattered in the gutters. It's a lot of work and HTML to do something simple.

Hmmmmm.

There has to be an easier way than this tsunami of classNames and nested div tags!

With **eliosin**, assuming the same parent element as above, we will contain the same content by following just two of **god**'s commandments:

1. **god** layouts shall target tagNames not classNames.
2. _pillar_ _hell_ _heaven_ shall be children of the same _container_.

We will have instead:

**Figure 2**

```html
<link src="god.css" />

<nav><a>Home</a></nav>
<!-- content left column -->
<img src="advert.svg" />
<!-- content right column -->
<h1>Content heading</h1>
<!-- content centre -->
<p>Content paragraph.</p>
<!-- content centre -->
```

**eliosin** styles and displays the content in Figure 2 in the same way as _bootscrap_ might display the same content from Figure 1; with no classNames and no nesting.

Because all the main tags are siblings, this can make developing dynamic websites easy, especially for SPAs. As long as you inject new tags into your page in the right order, no complicated templates are required. **god** will ensure every tag sits in the right place.

Anything that easy must be sinful!

### And there was light

<figure>
  <img src="/eliosin/icon/light/artwork/splash.jpg" target="_splash" alt="There was light">
</figure>

**god** lays out page content into three _prongs_ - pillar, heaven and hell - which collapse nicely on mobile screens. You decide which tags will fall into which _prongs_. A tagName can only appear in one _prong_ per stylesheet.

## god vs classes

Having read the first part **god vs bootscrap**, you know **eliosin** doesn't think much of HTML fluff.

**god** only cares about tagNames and she's only thinking about the wireframe. Remember the first two commandments?

1. **god** shall only be used for page layout. Style your own forms.
2. **god** layouts shall target tagNames not classNames.

So how does **god** work? There's no mystery. First god divided the page.

### First **god** divided the page

- **god** divides the page vertically into three spaces. Let's call them _prongs_ collectively.

Commandments 4 to 6 describe this layout.

- _pillar_ tags shall be laidout as an equal-width, centred column.
- _hell_ tags shall be laidout left and _heaven_ tags (illustrated in Figure 1 as "\$\$") shall be laidout right.
- No _hell_ _heaven_ tags shall be laidout higher than the following _pillar_.

### Then you decide who goes to hell

In **eliosin**'s settings file, _you_ list the tagNames in your chosen prong - putting them into the pillar, heaven or hell prongs as required.

## god vs asciiart

Roughly, using asciiart to sketch it out, this is what a page of plain HTML (e.g. Figure 2) would look like using **god**.

In the following example:

- `p` has been added to the pillar
- `aside` ... going to hell
- `blockquote` ... has been good... bound for heaven
- `footer` is an atheist

_NB. TagNames which appears in the HTML, but are not listed in prongs, are not bound by the god stylesheet._

**Figure 1: The HTML**

```html
<link src="god.css" />
<body>
  <aside>H E L L 1</aside>
  <blockquote>H E A V E N 1</blockquote>
  <p>P I L L A R A</p>
  <blockquote>H E A V E N 2</blockquote>
  <aside>H E L L 2</aside>
  <blockquote>H E A V E N 3</blockquote>
  <aside>H E L L 3</aside>
  <p>P I L L A R B</p>
  <footer>All rights (c) god</footer>
</body>
```

### How **god** will LAYOUT The HTML on a large screen

**Figure 2: god's layout of Figure 1: The HTML**

_NB. Gaps are not to scale._

```shell
<---------------------------------- pagewidth --------------------------------->

 --------------------   --------------------------------   --------------------
|                    | |                                | |                    |
|     H E L L  1     | |                                | |   H E A V E N  1   |
|                    | |         P I L L A R  A         | |                    |
 --------------------  |                                | |                    |
                       |                                |  --------------------
                       |                                |
                       |                                |
                       |                                |
                       |                                |
 --------------------   --------------------------------   --------------------
|                    | |                                | |                    |
|     H E L L  2     | |                                | |   H E A V E N  2   |
 --------------------  |         P I L L A R  B         | |                    |
 --------------------  |                                | |                    |
|                    | |                                | |                    |
|     H E L L  3     | |                                |  --------------------
|                    | |                                |  --------------------
|                    | |                                | |                    |
 --------------------  |                                | |   H E A V E N  3   |
                       |                                | |                    |
                       |                                |  --------------------
                        --------------------------------
--------------------------------------------------------------------------------
  All rights (c) god
--------------------------------------------------------------------------------
```

So let's look at this in light of the 7 commandments:

1. **god** shall only be used for page layout. Style your own forms.

2. ✅ See Figure 2

3. **god** layouts shall target tagNames not classNames.

4. ✅ See Figure 1

5. _pillar_ _hell_ _heaven_ shall be siblings of the same container.

6. ✅ See Figure 1

7. _pillar_ tags shall be laidout as an equal-width, centred column.

8. ✅ See Figure 2

9. _hell_ tags shall be laidout left and _heaven_ tags shall be laidout right.

10. ✅ See Figure 2

11. No _hell_ _heaven_ shall be laidout higher than follow sibling _pillar_.

12. ✅ See Figure 1 and Figure 2 (huh❔)

13. _pillar_ content shall wrap when overlapping _hell_ _heaven_.

14. ✅ But not illustrated

(huh ❔) Looking at Figure 1, `<aside> H E L L 2</aside>` is followed by the _pillar_ tag `<p> P I L L A R B</p>` which is why the "H E L L 2" box sits alongside "P I L L A R B" box.

### How **god** will LAYOUT The same HTML on a MOBILE screen

_NB. pillar always has a little bit of margin._

**Figure 3: god's layout of Figure 1: The HTML on small screens**

```shell
<----------- pagewidth ------------>

------------------------------------
|                                  |
|            H E L L  1            |
|                                  |
------------------------------------
|                                  |
|          H E A V E N  1          |
|                                  |
------------------------------------
 |                                |
 |         P I L L A R  A         |
 |                                |
------------------------------------
|                                  |
|          H E A V E N  2          |
|                                  |
------------------------------------
|                                  |
|            H E L L  2            |
|                                  |
------------------------------------
|                                  |
|          H E A V E N  3          |
|                                  |
------------------------------------
|                                  |
|            H E L L  3            |
|                                  |
------------------------------------
 |                                |
 |         P I L L A R  B         |
 |                                |
------------------------------------
------------------------------------
  All rights (c) god
------------------------------------
```

## The Pillars of Earth

<figure>
  <img src="/eliosin/icon/pillar/artwork/splash.jpg" target="_splash" alt="Pillars of creation">
</figure>

### The settings File

**or how god's word became law**

Having read the previous part **god vs asciiart**, you'll have an asciiart-picture in your mind of how the page will look. (And an asciiart picture is worth about 168.5 words.)

This is **eliosin**'s `settings.scss` - which you would normally override with your own. Compare this with _bootscrap_'s settings file!:

```scss
// BELIEVERS ==================
$container: (body);
$pillar: (h1, h2, p);
$heaven: (blockquote, figure);
$hell: (nav, aside);
// LAYOUT ==================
$god-space: 3%;
$heaven-width: 20%;
$hell-width: 20%;
$personal-space: 12px;
$transition: 3s;
// MEDIA BREAKS ==================
$god-loose: 1200px;
$god-cosy: 900px;
$god-tight: 600px;
// COLORS ==================
$heaven-color: lightgrey;
$pillar-color: black;
$hell-color: darkgrey;
$god-color: $pillar-color;
$god-blushed: lighten($god-color, 15);
// TEXT ==================
$god-h1: 500%;
$god-h6: 100%;
$god-p: 100%;
$god-spoke: $god-p * 1.1;
$god-head: sans-serif;
$god-body: serif;
// BASICS ==================
$god-border: 1px $god-blushed solid; // general purpose border
$inline-padding: 3px;
$toothpaste-color: "none";
```

### Settings worth highlighting

Remember when I said "First **god** divided the page" in the page before? This bit...

```scss
$god-space: 1%; // the pad (as a percentage to help scaling).
$hell-width: 20%; // gutter width left (as a percentage to help scaling).
$heaven-width: 30%; // gutter width right (as a percentage to help scaling).
```

... divided the page" by:

1. Making the width of elements with tagNames in _hell_ `$hell-width` 20% and tagNames in _heaven_ `$heaven-width` 30%.

2. Floating tagNames in _hell_ left and tagNames in _heaven_ right.

3. Fixing _pillar_ tagNames into a central column being:

4. `$pillar_width` = 100% - `$hell-width`% - `$heaven-width`%

5. Tidying it all for you.

`$god-space` is used to give the columns and their contents more horizontal space.

### More settings worth highlighting

Remember when I said "Then you decide who goes to _hell_"?

This setting is the bit where you decide who goes to _heaven_ or _hell_ or stays in the _pillar_:

```scss
$pillar: (p); /* p is in the pillar */
$heaven: (blockquote); /* blockquote is in heaven */
$hell: (aside); /* aside is in hell */
```

## Where Flesh becomes Sin

Now we can revisit **elioflesh**'s `flesh.json` file to complete the picture of how it can be tightly welded to **eliosin**.

```
{
  "name": { "believer": "h1" },
  "description": { "believer": "p" },
  "status": { "believer": "dl" },
  "Itemlist": {
    "believer": "dl",
    "itemListElement": {
      "name": { "believer": "dt" },
      "disambiguatingDescription": { "believer": "dd" }
    },
    "Person": {
      "fistName": { "believer": "fieldset" },
      "itemListElement": {
        "name": { "believer": "dt" },
        "disambiguatingDescription": { "believer": "dd" }
      }
    }
  }
}
```

**eliosin** applies css styles only to direct child tags of a parent element. **elioflesh**'s `flesh.json` directs it to render each

So that's **eliosin** . A three pronged approach to webpage layout leaving you, the HTML app developer, free to worry about other things.

## What's Next?

- [elioWay dogma](/dogma.html)

  - [eliothing dogma](/eliothing/dogma.html)
  - [eliobones dogma](/eliobones/dogma.html)
  - [elioflesh dogma](/eliothing/dogma.html)
  - eliosin dogma _(You are here)_
  - [elioWay dogma](/dogma.html) _(Read again. Does TEW deliver?)_
