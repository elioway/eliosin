# Contributing

```
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git submodule init
git submodule update
# or
set L adon eve god innocent sassy-fibonacciness generator-sin icon sins toothpaste bushido
for R in $L
    git submodule add git@gitlab.com:eliosin/$R.git
end
```

### TODOS

1. **brunch** instead of **gulp**?
2. **paradise-lost** or **lost** as possible new theme pack/replace **bushido** (use as framework group like **Hono**? )?
3. If we 2., do rename **sins** **seven-deadly** or **deadly**
