# Cheat HTML Tags

A list of Block level elements in HTML. Any of these would make excellent sinners:

- `address`
- `article`
- `aside`
- `blockquote`
- `canvas`
- `dd`
- `div`
- `dl`
- `dt`
- `fieldset`
- `figcaption`
- `figure`
- `footer`
- `form`
- `h1`
- `h6`
- `header`
- `hr`
- `li`
- `main`
- `nav`
- `noscript`
- `ol`
- `p`
- `pre`
- `section`
- `table`
- `tfoot`
- `ul`
- `video`

Remember... **eliosin** stylesheets _only_ targets the tag when it is a direct child of the parent block (aka _believers_). So you can use any of these tags nested further down or up the DOM tree - and they will behave as normal.
