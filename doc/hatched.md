# Hatched

**eliosin** was hatched `2018-02-18`. It's always been a core part of the idea. However, eliosin can stand apart from the rest of the elioVerse... at its core it is a classless CSS framework... been done before.

The strength of the project will result in simplification of HTML generation in **the elioWay**.

Take this whole website, for example. I am writing all this documentation in MARKDOWN. I don't want to have to write tag names and class names any more... nesting div tags deep inside each other like a some kind of perverted sexual act. I know that there are libraries which convert MARKDOWN to HTML. That's the whole point of it. You can get a conversion with basic tag names.

So **eliosin** works with basic tag names. The output of the converted MARKDOWN of this page will be a single _list_ of tags with content. Some of those tags will be `blockquote` tags. Some will be `code` tags. As I'm wrting this, I'm adding a `blockquote` above a paragraph it connects to because I know that in **eliosin** I have it set up to put blockquotes in _hell_ (hanging in the page margin on the left) and pictures in _heaven_ (hanging in the page margin on the right) and all the other tags laid out as a sturdy pillar of text down the page. Go and look. View source. You won't see many `className="trickyEndBox strongOnAWednesday shootMeNow"` attributes in this code.

I wrote this page in about an hour - it's basically a text file with some light "markup" in MARKDOWN. It had to be written - the computer couldn't do that. But you can be sure the **elioWay** will care of the rest of its journey from `File > Create New` to publication on some webserver and some website... I only want to do the things the computer can't do (for now - until our inevitable surfdom under an AI dictatorship).
