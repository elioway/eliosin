# eliosin Credits

## Artwork

- [wikimedia:Adam_and_Eve_humbly_pray](https://commons.wikimedia.org/wiki/File:Adam_and_Eve_humbly_pray_to_a_stern_God._Line_engraving._Wellcome_V0034182.jpg)

## Helpful

- [thecodercoder:gulp4 boilerplate](https://github.com/thecodercoder/frontend-boilerplate/blob/master/index.html) When upgrading to gulp 4, I started with this boilerplate
