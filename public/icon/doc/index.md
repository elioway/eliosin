<aside>
  <dl>
  <dd>He ceased; for both seemed highly pleased, and Death</dd>
  <dd>Grinned horrible a ghastly smile</dd>
</dl>
</aside>

**icon** is a resource bank for **elioWay** project icon.

At some point in the future we should create an **elioAngel** "iconpack builder" here, which does the following:

- Builds 72point elio star icon of specified size from a base image.
- Builds 24point elio star icon of specified size from a base image.
- Builds apple-touch-icon automatically.
- Combines svg icon into stylesheets, like icomoon does.
- Knit **icon** into the **eliosin** framework.
