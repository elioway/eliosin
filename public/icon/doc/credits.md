# icon Credits

## Artwork

- [wikimedia:Roadworks_sign](https://commons.wikimedia.org/wiki/File:Roadworks_sign.JPG)

## This was useful

- [randomhacks:convert-a-png-to-ico-file-ubuntu](https://www.randomhacks.co.uk/how-to-convert-a-png-to-ico-file-ubuntu-linux/)

## Worth a look!

- [codepen:jakob-e svg in scss](https://codepen.io/jakob-e/pen/doMoML)
- <https://github.com/feathericons/feather>
- <https://css-tricks.com/svg-sprites-use-better-icon-fonts/>
- <https://github.com/FWeinb/grunt-svgstore>
