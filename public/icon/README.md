![](https://elioway.gitlab.io/eliosin/icon/elio-icon-logo.png)

> Put a smiley on your face **the elioWay**

# icon ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "experimental")

Web icons and packs done **the elioWay**.

- [icon Documentation](https://elioway.gitlab.io/icon)

# Installing

- [Installing icon](https://elioway.gitlab.io/icon/installing.html)

# Nutshell

- [icon Quickstart](https://elioway.gitlab.io/eliosin/icon/quickstart.html)
- [icon Credits](https://elioway.gitlab.io/eliosin/icon/credits.html)

![](https://elioway.gitlab.io/eliosin/icon/apple-touch-icon.png)

# License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
