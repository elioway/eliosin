![](https://elioway.gitlab.io/eliosin/bushido/jin/elio-bushido-jin-logo.png)

# 仁

> through intense training and hard work the true warrior becomes quick and strong. they are not as most people. they develop a power that must be used for good. they have compassion. they help their fellow men at every opportunity. if an opportunity does not arise, they go out of their way to find one. benevolence, **Wikipedia**

**jin** means compassion. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/jin/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
