![](https://elioway.gitlab.io/eliosin/bushido/elio-bushido-chugi-logo.png)

# 忠義

> warriors are responsible for everything that they have done and everything that they have said and all of the consequences that follow. they are immensely loyal to all of those in their care. to everyone that they are responsible for, they remain fiercely true. **Wikipedia**

**chūgi** means duty and loyalty. A [bushido](https://gitlab.com/bushido/blob/master/doc/index.html) theme.

![](https://elioway.gitlab.io/eliosin/bushido/chugi/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
