![](https://elioway.gitlab.io/eliosin/bushido/yu/elio-bushido-yu-logo.png)

# 勇

> hiding like a turtle in a shell is not living at all. a true warrior must have heroic courage. it is absolutely risky. it is living life completely, fully and wonderfully. heroic courage is not blind. it is intelligent and strong. **Wikipedia**

**yū** means heroic courage. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/yu/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
