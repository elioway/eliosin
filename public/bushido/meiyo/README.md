![](https://elioway.gitlab.io/eliosin/bushido/meiyo/elio-bushido-meiyo-logo.png)

# 名誉

> warriors have only one judge of honor and character, and this is themselves. decisions they make and how these decisions are carried out are a reflection of who they truly are. you cannot hide from yourself. **Wikipedia**

**meiyo** means honour. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/meiyo/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
