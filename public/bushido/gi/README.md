![](https://elioway.gitlab.io/eliosin/bushido/gi/elio-bushido-gi-logo.png)

# 義

> be acutely honest throughout your dealings with all people. believe in justice, not from other people, but from yourself. to the true warrior, all points of view are deeply considered regarding honesty, justice and integrity. warriors make a full commitment to their decisions. **Wikipedia**

**gi** is righteousness. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/gi/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
